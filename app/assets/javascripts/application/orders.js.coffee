window.order_flow = window.order_flow or {}
((orders, $) ->
  
  orders.filterOrders = ->
    $(document).off "change", ".order_select"    
    $(document).on "change", ".order_select", ->
      $(".filter_form").submit()
      
  
  orders.refreshOrders = ->
    $(document).ready ->
      setInterval (->
        $(".filter_form").submit()
        return
      ), 15000
      return
  
  orders.changeStatus = ->
    $(document).off "click", ".change_status"    
    $(document).on "click", ".change_status", ->
      order_id = $(@).data("orderid")
      url =  "/orders/" + order_id + "/change_status"
      $.ajax
        url: url
        dataType: "script"
        type: "POST"
        success: ->
          $(".filter_form").submit()

) window.order_flow.orders = window.order_flow.orders or {}, jQuery
