class OrdersController < ApplicationController
  
  def index
    @orders = Order.all.order(updated_at: :desc)
  end
  
  def filter
    if params[:status] != "All"
      @orders = Order.where(status: params[:status])
    else
      @orders = Order.all
    end
    @orders = @orders.order(updated_at: :desc)
  end
  
  def change_status
    order = Order.find_by_id(params[:id])
    if order.present?
      order.status = order.reverse_status
      order.save
    end
    head :ok
  end
  
end
