module Api
class BaseController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json
  before_action :default_json

  def render_json(view_name)
    render view_name, formats: [:json], 
                     handlers: [:jbuilder], 
                       status: 200
  end
  
  def render_or_false(to_check, view)
    if to_check.present?
      render_json view
    else
      render json: false, status: 200
    end
  end
  
  protected

  def default_json
    request.format = :json if params[:format].nil?
  end

end
end
