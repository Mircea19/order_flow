module Api
class OrdersController < BaseController
 
  def create
    @order = Order.new(order_params)
    if @order.save
      render_or_false(@order, "api/orders/show")
    else
      render json: @order.errors.full_messages, status: 400 and return
    end
  end
  
  def update
    @order = Order.find_by_id(params[:id])
    if @order.present?
      if @order.update(order_params)
        render_or_false(@order, "api/orders/show")
      else
        render json: @order.errors.full_messages, status: 400 and return
      end
    end
  end
  
  def index
    @orders = Order.all
    render_or_false(@orders, "api/orders/index")
  end
 
  private
  
  def order_params
    params.require(:order).permit(:name, :description, :status)
  end
 
end
end
