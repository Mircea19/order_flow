class Order < ApplicationRecord
  
  STATUSES = ["pending", "delivered"]
  validates_presence_of :name
  validates :status, inclusion: {in: Order::STATUSES}
  scope :are_pending, -> {where(status: "pending")}
  scope :are_delivered, -> {where(status: "delivered")}
  
  def reverse_status
    self.status == "pending" ? "delivered" : "pending"
  end
  
  
end
