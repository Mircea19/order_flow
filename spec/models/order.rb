require 'rails_helper'

describe Order, type: :model do
  it "is invalid default" do
    order = Order.new
    expect(order).to_not be_valid
  end
  it "is invalid without a name" do
    order = Order.new
    order.status = "pending"
    expect(order).to_not be_valid
  end
  it "is invalid with status missing" do
    order = Order.new
    order.name = "IKEA"
    expect(order).to_not be_valid
  end
  it "is invalid without status in [pending, delivered]" do
    order = Order.new
    order.name = "IKEA"
    order.status = "registered"
    expect(order).to_not be_valid
  end
  it "is valid with name and status pending" do
    order = Order.new
    order.name = "IKEA"
    order.status = "pending"
    expect(order).to be_valid 
  end
  it "is valid with name and status delivered" do
    order = Order.new
    order.name = "IKEA"
    order.status = "delivered"
    expect(order).to be_valid 
  end
end
