Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # routes for web 
  resources :orders do 
    collection do
      post :filter
    end
    member do 
      post :change_status
    end
  end
  root "orders#index"
  
  # routes for the API's
  namespace :api, defaults: {format: :json} do
    resources :orders
  end
end
