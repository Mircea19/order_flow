# Order Flow

OrderFlow is a Rails 5.1.4 app that aims to revolutionize the flower order industry. 
It contains a Web Dashboard and some APIs created for an IOS app. 

## Running on your Machine

Note: In order to run it you must have Ruby and PostgreSQL installed on your machine. 
  
Get the source:

	$ git clone https://Mircea19@bitbucket.org/Mircea19/order_flow.git
	$ cd order_flow

Install the Gems:

	$ gem install bundler
	$ bundle install

Set up the Database for development and testing: 

	$ createdb orderflow_development
	$ createdb orderflow_test
	$ rake db:migrate db:test:prepare

Finally, run the server:

	$ rails s


	